# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2023/06/15 (Thu) 21:26:23 -03



[[ ! $- =~ i ]] && shopt expand_aliases | grep -vq on &&
echo -e "\nEm um script, a menos que seja executado por \e[1;32msource\e[0m, o uso de \e[1;32malias\e[0m exige habilitar a expansão de aliases:\n\e[1;37mshopt -s expand_aliases\e[0m\n"



benshmark-v6()
{

  local repeticoes=$1 fator unidade indefinidas plural1 plural2=ão plural3=á precisao=3
  local LC_ALL=C TIMEFORMAT=%lR

  shift

  # se o último parâmetro for '%', a classificação entre as soluções será em porcentagem, caso contrário, será em "número de vezes"
  [[ ${@: -1} == % ]] &&
  {
    fator=100 unidade=%
    set -- ${@:1:$(($#-1))}
  } || fator=1 unidade=vez

  # se a rotina for chamada sem parâmetros ou ao número de repetições não for um número natural, finaliza com o modo de uso
  [[ $* && $repeticoes =~ ^[0-9]+$ ]] ||
  {
    echo "Uso: $FUNCNAME <repetições> <função> <função> [...] [%]" >&2
    return 255
  }

  # confere se todos os nomes passados como parâmetro são de alguma função existente
  declare -F $* &> /dev/null ||
  {
    indefinidas=( $(grep -vf <(declare -F $*) <(printf '%s\n' $*)) )
    (( ${#indefinidas[*]} > 1 )) && plural1=s plural2=ões plural3=ão
    echo "A$plural1 funç$plural2 (${indefinidas[*]}) não est$plural3 definida$plural1." >&2
    return 1
  }

  echo; sudo true

  echo

  # executa $repeticoes vezes cada $solucao armazenada em uma funcao
  for solucao
  {
    echo 3 | sudo tee /proc/sys/vm/drop_caches > /dev/null
    echo -n "$solucao  " >&2
    IFS=ms read minutos segundos <<< $( { time for ((i=0;i<repeticoes;i++)); { $solucao; } &> /dev/null; } 2>&1 )
    printf -v tempo "%02i:%06.3f" $minutos $segundos
    echo ${tempo/./,} >&2
    echo "$minutos*60*10**precisao+10#${segundos/./}*10**(precisao-3) $solucao"
  } |
  sort -V | # coloca o tempo de execução das repetições de cada solução em ordem crescente
  {

    # assume a primeira da lista como a melhor $solucao (menor tempo de execução de todas as repetições)
    read melhortempo melhorsolucao
    echo -e "\n$((n=1))º  $melhorsolucao"
    melhortempo=$((10#$melhortempo))

    # calcula a proporção entre o tempo de cada uma das outras soluções com o tempo da melhor (em porcentagem se for o caso)
    while read tempo solucao
    do

      echo -n "$((++n))º  $solucao/$melhorsolucao  "

      (( melhortempo > 0 )) &&
      {

            tempo=$((10#$tempo))
        proporcao=$((10**precisao*fator*(tempo-melhortempo)/melhortempo))
        printf -v proporcao %03i $proporcao
        (( 10#$proporcao > 0 )) &&
        {

        # declare -p proporcao >&2
        # declare -p precisao >&2

          inteiro=${proporcao::-$precisao}
          decimal=${proporcao: -$precisao:$precisao}
          (( inteiro > 1 )) && [[ $unidade == vez ]] && plural=es

        # declare -p inteiro >&2

          (( ${#inteiro} > 3 )) &&
          milhar=${inteiro::-$precisao}. centena=${inteiro: -$precisao:$precisao} ||
          { milhar=; (( ${#inteiro} > 0 )) && centena=$inteiro || centena=; }

          printf "%$((25+${#plural}))s\n" "$milhar${centena:-0},$decimal $unidade$plural mais lenta"

        } || echo =

      } || echo ∞

    done

  }

  echo

}



# EOF
