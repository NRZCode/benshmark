# vim: ft=sh

# Arkanon <arkanon@lsd.org.br>
# 2021/07/26 (Mon) 19:02:23 -03



benshmark-v1()
{
  local LC_ALL= LC_NUMERIC=C TIMEFORMAT=%lR L=$1 s; shift
  for s
  {
    echo -n "$s  "
    time for ((i=0;i<L;i++)); { $s; } &> /dev/null
  }
}



# EOF
